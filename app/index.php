<?php include('../template/header.php'); ?>
<div class="container">
  
  <div class="alert alert-info" role="alert">Prueba de selección DATACRM</div>
  
  <!--  listar Contactos -->
  <button id="btnConsutar" class="btn btn-primary">Consultar Contactos</button> <span style="visibility: hidden ;" id="spinner"><img src="../resource/img/spinner.gif" alt=""></span>
  <table id="tablaContactos" class="table table-striped table-hover">
    <thead>
      <tr>
        <th> <i class="fa fa-user fa-fw"></i> Id</th>
        <th> <i class="fa fa-at fa-fw"></i> Contact</th>
        <th> <i class="fa fa-venus-mars fa-fw"></i> Last Name</th>
        <th> <i class="fa fa fa-briefcase fa-fw"></i> Date creation</th>
      </tr>
    </thead>
    <tbody>
      <!-- Modificado por ajax -->
    </tbody>
  </table>
</div>
<?php include('../template/footer.php'); ?>
<script src="js/contacts.js?sin_cache=<?php echo md5(time()); ?>"></script>