<?php

if ( isset( $_GET['opcion'] ) ) {
    include_once('../models/contacts.php');
    $obj = new Contacts();
    switch ($_GET['opcion']) {
        case 'getContacts':
                $data = $obj->getContacts();
            break;
        default:
                $data = json_encode( array( 'success' => false, 'error' => array( 'code' => 'Metodo no permitido' ) ) ) ; // Se conserva la estructura de respuesta del api
            break;
    }
    echo $data;
}else{
    header('Location: ../index.php');
    exit;
}
