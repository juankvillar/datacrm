<?php

class Contacts
{
	public $clvAcceso = NULL;
	public $query = 'select id, contact_no, lastname, createdtime from Contacts;';// se traen solo lo capos reqeuridos por el documento

	public function __construct()
	{
		$this->clvAcceso = '3DlKwKDMqPsiiK0B'; // clave de acceso paraingresoa a la aplicación
		$this->query = str_replace(' ', '%20', $this->query);
	}

	public function getchallenge($operation,  $username) // función que obtiene el token inicial
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://develop.datacrm.la/anieto/anietopruebatecnica/webservice.php?operation=' . $operation . '&username=' . $username);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

	static function login($operation,  $username, $accessKey) //Función que genera el sessionName
	{
		
		$fields = array('operation' => $operation, 'username' => $username, 'accessKey' => $accessKey);
		$fields_string = http_build_query($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://develop.datacrm.la/anieto/anietopruebatecnica/webservice.php');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

	public function getContacts() // función ue se loguea y obtiene los datos del api
	{

		// Paso 1, obtener el token
		$res = $this->getchallenge('getchallenge',  'prueba');
		$res = json_decode($res);

		// Paso 2 generar el sessionName
		$accessKey = md5($res->result->token . $this->clvAcceso);
		$arrSesi = Contacts::login('login', 'prueba', $accessKey);
		$arrSesi = json_decode($arrSesi);
		$sessionName = $arrSesi->result->sessionName;
		
		// Enviar el query
		$operation = 'query';
		$sessionName = $sessionName;
		$query = $this->query;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://develop.datacrm.la/anieto/anietopruebatecnica/webservice.php?operation=' . $operation . '&sessionName=' . $sessionName . '&query=' . $query);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}
}
