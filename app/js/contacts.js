$('#btnConsutar').click(function (e) {
    e.preventDefault();
    $('#spinner').css('visibility', 'visible');
    $.ajax({
        type: 'get',
        url: 'controllers/contacts.php',
        dataType: 'json',
        data: { opcion: "getContacts"},
    }).done(function (data) {
        $('#spinner').css('visibility', 'hidden');
        if (data.success) {
            swal( 'Operación exitosa', '', 'success');
            let tabla = '';

            data.result.forEach(function (d, index) {
                tabla += `<tr>
                            <td>`+ d.id + `</td>
                            <td>`+ d.contact_no + `</td>
                            <td>`+ d.lastname + `</td>
                            <td>`+ d.createdtime + `</td>
                        </tr>`;
            });
            $('#tablaContactos tbody').html(tabla);
        }else{
            swal( data.error.code, '', 'error');
        }
    })
        .fail(function (data) {
            $('#spinner').css('visibility', 'hidden');
            swal('No se realizó ningún cambio', '', 'error');
        })

});